<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/register', 'AuthController@register');
Route::post('/kirim', 'AuthController@kirim');
Route::get('/welcome', 'AuthController@welcome');
Route::get('/adminlte/master', 'MasterController@master');



Route::get('/', 'PertanyaanController@home');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::get('pertanyaan', 'PertanyaanController@index');
Route::post('pertanyaan', 'PertanyaanController@store');
Route::get('pertanyaan/{id}', 'PertanyaanController@show');
Route::get('pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('pertanyaan/{id}', 'PertanyaanController@update');
Route::Delete('pertanyaan/{id}', 'PertanyaanController@destroy');