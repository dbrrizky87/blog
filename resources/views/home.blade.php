@extends('adminlte.master')

@section('content')
<section class="content">

	<section class="content">

		<!-- Default box -->
		<div class="card">
		  <div class="card-header">
			<b><h3 class="mt-4">SELAMAT DATANG DI FORUM PROGRAMER SANBERCODE</h3></b>
  
			<div class="card-tools">
			  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fas fa-minus"></i></button>
			  
			</div>
		  </div>
		  <div class="card-body">
			<p>Forum ini berisikan diskusi tanya jawab bagi para member Sanbercode. Selamat bergabung</p>
		  </div>
		  <!-- /.card-body -->
		  <div class="card-footer">
			<i><p><b>*Note: Mohon untuk mempergunakan forum ini dengan bijak!!!</b></p></i>
		  </div>
		  <!-- /.card-footer-->
		</div>
		<!-- /.card -->
  
	  </section>

@endsection