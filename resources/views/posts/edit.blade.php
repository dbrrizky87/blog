@extends('adminlte.master')

@section('content')
<section class="content">

    <div class="card">
        <div class="card-header">
            <center><h4>EDIT PERTANYAAN</h4></center>
          </div>
    <form class="form-horizontal" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
            @csrf
            @method('PUT')
          <div class="container mt-4">
            <div class="card">
              <div class="card-header bg-info">Pertanyaan</div>
              <div class="card-body">
                <input class="form-control form-control-sm" name="judul" value="{{$pertanyaan->judul}}" placeholder="Judul">      
            </div> 
              <div class="card-footer">
                    <div class="input-group input-group-sm mb-0">
                      <input class="form-control form-control-sm" name="isi" value="{{$pertanyaan->isi}}" placeholder="Isi Pertanyaan">
                    </div>      
            </div>
            </div>
          </div>
        <div class="card-body">
            <div class="input-group-append">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
        </div>
    </form>
      </div>
    
  </section>

@endsection