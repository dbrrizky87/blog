@extends('adminlte.master')

@section('content')
<section class="content">

    <div class="card">
        <div class="card-header">
            <center><h2>FORUM TANYA JAWAB PROGRAMMER</h2></center>
          </div>
        <div class="card-body">
          <a href="pertanyaan/create" class="btn float-right btn-info"> <i class="fa fa-plus mr-2"></i>Buat Pertanyaan</a>
        </div>
      </div>
      
	  <!-- Default box -->
	  <div class="card-body">
		@if (session('success'))
		<div class="alert alert-success ">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{ session('success') }}
		@endif
	  </div>
	  
	<div class="card-body">
		<div class="tab-content">
		  <div class="active tab-pane" id="activity">
            <!-- Post -->
            @forelse ($pertanyaan as $item => $value)
                
         
			<div class="post clearfix">
			  <div class="user-block">
				<img class="img-circle img-bordered-sm" src="{{asset('/adminlte/dist/img/user7-128x128.jpg')}}" alt="User Image">
				<span class="username">
				  <a href="#">Garry Lean</a>
				  <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
				</span>
				<span class="description">post dikirim - {{$value->tanggal_dibuat}}</span>
              </div>
              <form action="pertanyaan/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
              <input type="submit" class="btn float-right btn-danger btn-sm" onclick="return confirm('yakin mau di hapus nih?')" value="Hapus">
            </form>
              <a href="pertanyaan/{{$value->id}}/edit" class="btn float-right btn-success btn-sm mr-2">Edit</a>
              <a href="pertanyaan/{{$value->id}}" class="btn float-right btn-info btn-sm mr-2">Detail</a>
              <!-- /.user-block -->
              <div>
                  <h3>{{$value->judul}}</h3>
            </div>
			  <p>
				{{$value->isi}}
			  </p>
			  <p>
				<a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Bagikan</a>
				<a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Suka</a>
				<span class="float-right">
				  <a href="#" class="link-black text-sm">
					<i class="far fa-comments mr-1"></i> Komentar (5)
				  </a>
				</span>
			  </p>

			  <form class="form-horizontal">
				<div class="input-group input-group-sm mb-0">
				  <input class="form-control form-control-sm" placeholder="komentar">
				  <div class="input-group-append">
					<button type="submit" class="btn btn-primary">Kirim</button>
				  </div>
				</div>
			  </form>
			</div>
            <!-- /.post -->
            @empty
            <p>Tidak ada Pertanyaan</p>
                
            @endforelse
		  </div>


		  <!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	  </div>

  </section>

@endsection