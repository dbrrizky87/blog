@extends('adminlte.master')

@section('content')
<section class="content">

    <div class="card">
        <div class="card-header">
            <center><h4>BUAT PERTANYAAN</h4></center>
          </div>
    <form class="form-horizontal" action="/pertanyaan" method="POST">
            @csrf
          <div class="container mt-4">
            <div class="card">
              <div class="card-header bg-info">Pertanyaan</div>
              <div class="card-body">
                <input class="form-control form-control-md " name="judul" placeholder="Judul">    
                @error('judul')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror  
            </div> 
              <div class="card-body">
                  
                      <input class="form-control form-control-md" name="isi" placeholder="Isi Pertanyaan">
                      @error('isi')
                      <div class="alert alert-danger mt-2">
                        {{ $message }}
                      </div>
                      @enderror
                         
            </div>
            </div>
          </div>
        <div class="card-body">
            <div class="input-group-append">
                <button type="submit" class="btn btn-info">Kirim</button>
              </div>
        </div>
    </form>
      </div>
    
  </section>

@endsection