@extends('adminlte.master')

@section('content')
<section class="content">

    <div class="card">
        <div class="card-header">
            <center><h3>DETAIL PERTANYAAN</h3></center>
          </div>
      </div>
    
      <!-- Default box -->
	<div class="card-body">
		<div class="tab-content">
		  <div class="active tab-pane" id="activity">
            <!-- Post -->
                
			<div class="post clearfix">
			  <div class="user-block">
				<img class="img-circle img-bordered-sm" src="{{asset('/adminlte/dist/img/user7-128x128.jpg')}}" alt="User Image">
				<span class="username">
				  <a href="#">Sarah Ross</a>
				  <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
				</span>
				<span class="description">post dikirim - {{$pertanyaan->tanggal_dibuat}}</span>
              </div>
              <a href="/pertanyaan" class="btn float-right btn-info btn-sm">Kembali</a>
              <!-- /.user-block -->
              <div>
                  <h3>{{$pertanyaan->judul}}</h3>
            </div>
			  <p>
				{{$pertanyaan->isi}}
			  </p>
			  <p>
				<a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Bagikan</a>
				<a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Suka</a>
				<span class="float-right">
				  <a href="#" class="link-black text-sm">
					<i class="far fa-comments mr-1"></i> Komentar (5)
				  </a>
				</span>
			  </p>

			  <form class="form-horizontal">
				<div class="input-group input-group-sm mb-0">
				  <input class="form-control form-control-sm" placeholder="komentar">
				  <div class="input-group-append">
					<button type="submit" class="btn btn-danger">Kirim</button>
				  </div>
				</div>
			  </form>
			</div>
            <!-- /.post -->

		  </div>


		  <!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	  </div>

  </section>

@endsection