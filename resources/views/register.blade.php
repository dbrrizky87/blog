<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Form</title>
</head>
<body>
	<form action="/kirim" method="POST">
    @csrf
    
	<h1>Buat Akun Baru!</h1>
	<h3>Sign Up Form</h3>

	<br><br>
	<label>First Name:</label>
	<br><br>
	<input type="text" name="first_name">

	<br><br>
	<label>Last Name:</label>
	<br><br>
	<input type="text" name="last_name">
	
	<br><br>
	<label>Gender:</label>
	<br><br>
	<input type="radio" name="gender" value="0">Male<br>
	<input type="radio" name="gender" value="1"> Female<br>
	<input type="radio" name="gender" value="2">Other

	<br><br>
	<label for="nasionality">Nasionality:</label>
	<br><br>
	<select>
		<option value="indonesian">Indonesian</option>
		<option value="singaporean">Singaporean</option>
		<option value="malaysian">Malaysian</option>
	</select>
	<br><br>

	<label>Language Spoken:</label>
	<br><br>
	<input type="checkbox" name="language" value="0">Bahasa Indonesia<br>
	<input type="checkbox" name="language" value="1">English<br>
	<input type="checkbox" name="language" value="2">Other
	<br><br>

	<label>Bio:</label>
	<br><br>
		<textarea name="biografi" id="bio" cols="30" rows="10"></textarea>
		<br><br>
		<input type="submit" value="Sign Up">
		<br><br>
	</form>
	<br><br>
	<br><br>


</body>
</html>