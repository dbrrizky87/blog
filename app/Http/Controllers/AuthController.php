<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register() {
        return view ('register');
    }

    public function welcome() {
        return view ('welcome');
    }

    public function kirim(Request $request) 
    {
        $first_name = strtoupper ($request->first_name);
        $last_name = strtoupper ($request->last_name);
        return view ('welcome', ['first_name'=>$first_name],['last_name'=>$last_name] );
    }


}
