<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class PertanyaanController extends Controller
{

    public function home(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('home');
    }
    //
    public function create(){
        return view('posts.create');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('posts.index', compact('pertanyaan'));
    }

    public function store(Request $request){

        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);


        $query = DB::table('pertanyaan')->insert([
            'judul'=> $request['judul'],
            'isi' => $request['isi']
            ]);
        return redirect ('pertanyaan')->with('success', 'Pertanyaaan berhasil di posting!!!');
        }

        public function show($id)
        {
            $request->validate([
                'judul' => 'required|unique:pertanyaan',
                'isi' => 'required'
            ]);

            $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
            return view('posts.show', compact('pertanyaan'));
        }

        public function edit($id)
        {
            $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
            return view('posts.edit', compact('pertanyaan'));
            // dd($pertanyaan); 
        }

        public function update($id, Request $request){
            $query = DB::table('pertanyaan')
                            ->where('id', $id)
                            ->update([
                                'judul' => $request['judul'],
                                'isi' => $request['isi']
                            ]);
                                return redirect ('/pertanyaan')->with('success', 'Pertanyaan berhasil diubah!!!');

        }

        public function destroy($id){
            $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->delete();
            return redirect ('/pertanyaan')->with('success', 'Pertanyaan berhasi di hapus!!!');
        } 

        
    }





